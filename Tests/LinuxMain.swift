import XCTest

import RFViewUtilitiesTests

var tests = [XCTestCaseEntry]()
tests += RFViewUtilitiesTests.allTests()
XCTMain(tests)
