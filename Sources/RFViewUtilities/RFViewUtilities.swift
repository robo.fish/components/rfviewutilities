//
//  RFViewUtilities.swift
//
//  Created by Kai Oezer on 23.11.20.
//  Copyright © 2020 Robo.Fish UG. All rights reserved.
//

import UIKit

// A helpful list of display resolutions is available at https://ios-resolution.com/

public enum DisplayWidthClass : CGFloat
{
	case unknown = -1

	case w320 = 320    //   320 x  568 @ 2x | iPhone 5, 5s, SE, iPod touch
	case w360 = 360    //   360 x  780 @ 3x | iPhone 12 mini
	case w375 = 375    //   375 x  812 @ 3x | iPhone X, Xs, 11 Pro
	                   //   375 x  667 @ 2x | iPhone 6, 7, 8, 9, SE 2nd Gen
	case w390 = 390    //   390 x  844 @ 3x | iPhone 12, 12 Pro
	case w414 = 414    //   414 x  896 @ 3x | iPhone Xs Max, 11 Pro Max
	                   //   414 x  896 @ 2x | iPhone XR, 11
	                   //   414 x  736 @ 3x | iPhone 6 Plus, 6s Plus, 7 Plus, 8 Plus
	case w428 = 428    //   428 x  926 @ 3x | iPhone 12 Pro Max

	case w768 = 768    //   768 x 1024 @ 2x | iPad 3, 4, Air, Air 2, mini 2, mini 3, mini 4, 9.7-inch Pro
	case w810 = 810    //   810 x 1080 @ 2x | 10.2-inch iPad
	case w834 = 834    //   834 x 1112 @ 2x | 10.5-inch iPad Pro, iPad Air 3
										 //   834 x 1194 @ 2x | 11-inch iPad Pro
	case w1024 = 1024  //  1024 x 1366 @ 2x | 12,9-inch iPad Pro
}

public enum DisplayHeightClass : CGFloat
{
	case unknown = -1
	case h568 = 568    //   320 x  568 @ 2x | iPhone 5, 5s, SE
	case h667 = 667    //   375 x  667 @ 2x | iPhone 6, 7, 8, 9, SE 2nd Gen
	case h736 = 736    //   414 x  736 @ 2x | iPhone 6 Plus, 6s Plus, 7 Plus, 8 Plus
	case h780 = 780    //   360 x  780 @ 3x | iPhone 12 mini
	case h812 = 812    //   375 x  812 @ 3x | iPhone X, Xs, 11 Pro
	case h844 = 844    //   390 x  844 @ 3x | iPhone 12, 12 Pro
	case h896 = 896    //   414 x  896 @ 3x | iPhone Xs Max, 11 Pro Max
	                   //   414 x  896 @ 2x | iPhone XR, 11
	case h926 = 926    //   428 x  926 @ 3x | iPhone 12 Pro Max

	case h1024 = 1024  //   768 x 1024 @ 2x | iPad 3, 4, Air, Air 2, mini 2, mini 3, mini 4, 9.7-inch Pro
	case h1080 = 1080  //   810 x 1080 @ 2x | 10.2-inch iPad
	case h1112 = 1112  //   834 x 1112 @ 2x | 10.5-inch iPad Pro, iPad Air 3
	case h1194 = 1194  //   834 x 1194 @ 2x | 11-inch iPad Pro
	case h1366 = 1366  //  1024 x 1366 @ 2x | 12,9-inch iPad Pro
}

public func deviceDisplayWidthClass() -> DisplayWidthClass
{
	DisplayWidthClass(rawValue: UIScreen.main.bounds.width) ?? .unknown
}

public func deviceDisplayHeightClass() -> DisplayHeightClass
{
	DisplayHeightClass(rawValue: UIScreen.main.bounds.height) ?? .unknown
}
